import 'package:flutter/material.dart';

//
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    //将button行的小部件 封装成一个方法
    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
            margin: const EdgeInsets.only(top: 8),
            child: Text(
              label,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: color,
              ),
            ),
          ),
        ],
      );
    }

    //应用主题颜色
    Color color = Theme.of(context).primaryColor;
    //组装整个button行
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.call, 'CALL'),
          _buildButtonColumn(color, Icons.near_me, 'ROUTE'),
          _buildButtonColumn(color, Icons.share, 'SHARE'),
        ],
      ),
    );


    //文字区域
    Widget textSection = Container(
      padding: EdgeInsets.all(32),
      child: Text(
        '首先说一下FIRST集的生成，'
            '这个就要看产生式右部对应的首字母的“终结符”的个数的表现了，'
            '例如：A-> +TA|-TA|k   所以 A的FIRST集为+-k ;  '
            ' 同理B->*FB|/FB|k,所以B的FIRST集是*/k;  D的FIRST集是xyz;    '
            '接着我们再分析F，对F分析可以得FIRST集是  (  和  D的FIRST的并集，即(xyz，'
            '同理可以得到T和E的FIRST都是(xyz。',
        softWrap: true,
      ),
    );

    return new MaterialApp(
        title: 'ceshi',
        home: Scaffold(
            appBar: AppBar(
              title: Text('页面布局练习'),
            ),
            body: Column(
              children: <Widget>[
                //图片区域
                Expanded(
                  child:Image.asset(
                    'images/tupian.png',
                    width: 600,
                    height: 240,
                    fit: BoxFit.cover,
                  ),
                ),


                //标题行
                TitleSection(),
                //按钮行
                buttonSection,
                //文字区域
                textSection
              ],
            )));
  }
}


//将标题行创建成一个类
class TitleSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Oeschinen Lake Campground',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  'Kandersteg, Switzerland',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          /*3*/
//          Icon(
//            Icons.star,
//            color: Colors.red[500],
//          ),
//          Text('41'),
        FavoriteWidget()
        ],
      ),
    );
  }
}


//定义星星和文字的组件
class FavoriteWidget extends StatefulWidget {
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget> {
  //下划线开头的是私有变量
  bool _isFavoriteWidget = true;
  int _favoriteCount = 41;


  //定义一个点击方法
  void _toggleFavorite(){
    //调用setState 重绘
    setState(() {
      if(_isFavoriteWidget){
        _favoriteCount -= 1;
        _isFavoriteWidget = false;
      }else{
        _favoriteCount +=1;
        _isFavoriteWidget = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(0.0),
          child: IconButton(icon: (_isFavoriteWidget ?Icon(Icons.star) :Icon(Icons.star_half)), onPressed: _toggleFavorite)
        ),
        SizedBox(
          child: Container(
            child: Text('$_favoriteCount'),
          ),
        )
      ],
    );
  }
}
